package ro.infoiasi.gpxtrace;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;

import javax.swing.JComponent;

public class ImageComp extends JComponent {

	Image img;	
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		
		Graphics2D g2 = (Graphics2D)g;		
		
		if(img != null) {
			g2.drawImage(img, 0, 0, new ImageObserver() {				
				@Override
				public boolean imageUpdate(Image img, int infoflags, int x, int y,
						int width, int height) {
					repaint();
					return false;
				}
			});
		}
	}
	
}
