package ro.infoiasi.gpxtrace.maploading;

import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Collection;

import javax.imageio.ImageIO;

import ro.infoiasi.gpxtrace.maploading.exceptions.DrawException;

public class GoogleMapAdapter extends Map {
	
	private static final String API_KEY = "AIzaSyB8I205N-5MJBeS76umrc6Z2LInJyyeryI";
	
	private String baseURL = "http://maps.googleapis.com/maps/api/staticmap?key=" + API_KEY;
	private String requestURL = baseURL;
	
	private Toolkit toolkit;
	
	public GoogleMapAdapter(Toolkit toolkit) {
		this.toolkit = toolkit;
	}

	@Override
	public void addPolyline(Collection<Location> locations) throws DrawException {		
		Color c = getLineColor();
		String hexColor = Integer.toHexString(c.getRGB() & 0xffffff);
		
		while(hexColor.length() < 6) {
			hexColor = "0" + hexColor;
		}
		
		hexColor = "0x" + hexColor;
		
		requestURL += "&path=color:" + hexColor + "|" + "weight:" + getLineWidth();
		for(Location location : locations) {
				requestURL += "|" + location.getLatitude() + "," + location.getLongitude();
		}
	}

	@Override
	public void addPolygon(Collection<Location> locations) throws DrawException {		
		Color c = getLineColor();
		String hexColor = Integer.toHexString(c.getRGB() & 0xffffff);
		
		while(hexColor.length() < 6) {
			hexColor = "0" + hexColor;
		}
		
		hexColor = "0x" + hexColor;
		
		requestURL += "&path=fillcolor:"+hexColor+"|color:" + hexColor + "|" + "weight:" + getLineWidth();
		for(Location location : locations) {
				requestURL += "|" + location.getLatitude() + "," + location.getLongitude();
			}
	}

	@Override
	public void addPoint(Location myPoint) {
		requestURL += "&markers=";
		
		Color c = getLineColor();
		String hexColor = Integer.toHexString(c.getRGB() & 0xffffff);
		
		while(hexColor.length() < 6) {
			hexColor = "0" + hexColor;
		}
		
		hexColor = "0x" + hexColor;
		
		requestURL += "color:" + hexColor + "%7C" + myPoint.getLatitude() + "," + myPoint.getLongitude();
	}

	@Override
	public void zoom(Location myPoint) {
		requestURL += "&center=" + myPoint.getLatitude() + "," + myPoint.getLongitude();		
	}

	@Override
	public Image drawMap() throws DrawException {		
		requestURL += "&size=" + getMapSize().width + "x" + getMapSize().height;
		requestURL += "&zoom=" + getZoomLevel();
		
		Image result = null;
		
		try {					
			URL url = new URL(requestURL);
			URLConnection conn = (HttpURLConnection)url.openConnection();			
			
			result = ImageIO.read(conn.getInputStream());		
		} 
		catch (Exception e) {
			throw new DrawException(e);			
		}
				
		requestURL = baseURL;
		return result;
	}

}
