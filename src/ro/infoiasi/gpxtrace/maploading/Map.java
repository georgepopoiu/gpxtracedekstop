package ro.infoiasi.gpxtrace.maploading;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.Collection;

import ro.infoiasi.gpxtrace.maploading.exceptions.DrawException;
import ro.infoiasi.gpxtrace.maploading.exceptions.LoadMapException;

/**
 * Class that can be used to manage a map independently of its type.
 * @author george
 */
public abstract class Map {	
	
	/**
	 * Static method responsible for loading a map.
	 * 	 
	 * @param mapType The type of the map we want to load.
	 * @return A Map object that can be used to manipulate the map.
	 * @throws LoadMapException Call getMessage() on the returned exception to see why the loading failed.
	 */
	public static Map loadMap(MapType mapType, Toolkit toolkit) throws LoadMapException {		
		
		switch(mapType) {
			case GoogleMap:
				return new GoogleMapAdapter(toolkit);
			default:
				return null;
			
		}
	}
	
	/**
	 * Draws a polyline on the map using the given locations.
	 * 
	 * @param locations The locations to be used in order to draw the polyline.
	 * @throws DrawException Use e.getMessage() to see the reason dor the exception.
	 */
	public abstract void addPolyline(Collection<Location> locations) throws DrawException;
	
	
	/**
	 * Draws a polygon on the map using the given location
	 * @param locations The locations that will be used to draw the polygon
	 * @throws DrawException In case it appears an exception we handle it in DrawException class with the method e.getMessage
	 */
	public abstract void addPolygon(Collection<Location> locations) throws DrawException;
	
	
	/**
	 * Method responsible for drawing points on map
	 * 
	 * @param myPoint Coordinates for drawing a marker
	 */
	public abstract void addPoint(Location myPoint);
	
	
	/**
	 * Method responsible for zooming the map to a specific location
	 * 
	 * @param myPoint Coordinates for center
	 */
	public abstract void zoom(Location myPoint);
	
	
	/**
	 * Draws the map after all parameters have been set.
	 * @return 
	 */
	public abstract Image drawMap() throws DrawException;
	
		
	private int lineWidth = 10;	
		
	/**
	 * Sets the width of the line that will be drawn on the map by the drawPolyline and drawPolygon methods.
	 * @param lineWidth
	 */
	public void setLineWidth(int lineWidth) {
		if(lineWidth <= 0)
			return;
		
		this.lineWidth = lineWidth;
	}
	
	/**
	 * Gets the width of the line that will be drawn on the map by the drawPolyline and drawPolygon methods.
	 * @return
	 */
	public int getLineWidth() {
		return lineWidth;
	}
	
	
	private Color lineColor = Color.BLUE;
	
	/**
	 * Sets the color of the line that will be drawn on the map by the drawPolyline and drawPolygon methods.
	 * @param color
	 */
	public void setLineColor(Color color) {
		this.lineColor  = color;
	}
	
	/**
	 * Gets the color of the line that will be drawn on the map by the drawPolyline and drawPolygon methods.
	 * @return
	 */
	public Color getLineColor() {
		return lineColor;
	}
	
	
	int zoomLevel = 5;
	
	/**
	 * Gets the current zoom level of the map.
	 * @return
	 */
	public int getZoomLevel() {
		return zoomLevel;
	}

	/**
	 * Sets the current zoom level of the map.
	 * @param zoomLevel
	 */
	public void setZoomLevel(int zoomLevel) {
		if(zoomLevel < 0)
			return;
		
		this.zoomLevel = zoomLevel;
	}
	
	Dimension mapSize = new Dimension(300, 300);
	
	/**
	 * Gets the dimension object with the width and height of the image representing the map.
	 * @return java.awt.Dimension
	 */
	public Dimension getMapSize() {
		return new Dimension(mapSize);
	}
			
	/**
	 * 
	 * @param d
	 */
	public void setMapSize(Dimension d) {
		mapSize.width = d.width;
		mapSize.height = d.height;
	}		
}

