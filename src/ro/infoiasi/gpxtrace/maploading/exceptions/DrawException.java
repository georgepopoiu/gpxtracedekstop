package ro.infoiasi.gpxtrace.maploading.exceptions;

public class DrawException extends Exception {

	public DrawException() {
		// TODO Auto-generated constructor stub
	}

	public DrawException(String detailMessage) {
		super(detailMessage);
		// TODO Auto-generated constructor stub
	}

	public DrawException(Throwable throwable) {
		super(throwable);
		// TODO Auto-generated constructor stub
	}

	public DrawException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
		// TODO Auto-generated constructor stub
	}

}
