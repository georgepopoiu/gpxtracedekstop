package ro.infoiasi.gpxtrace;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import ro.infoiasi.gpxtrace.maploading.Location;
import ro.infoiasi.gpxtrace.maploading.Map;
import ro.infoiasi.gpxtrace.maploading.MapType;
import ro.infoiasi.gpxtrace.maploading.exceptions.LoadMapException;

public class TestFrame extends JFrame {
	
	Image img;
	ImageComp comp = new ImageComp();
	
	public TestFrame() {
		setTitle("GPXTrace Desktop Test");
		setSize(700, 700);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		
		try {			
			//incarc harta
			Map map = Map.loadMap(MapType.GoogleMap, getToolkit());
			
			//adaug o locatie
			map.addPoint(new Location(21.291, -157.821));
			
			//setez dimensiunea hartii
			map.setMapSize(new Dimension(800, 600));
			
			//setez nivelul de zoom
			map.setZoomLevel(3);
			
			map.zoom(new Location(-1000.100, 108.111));
			
			ArrayList<Location> locations = new ArrayList<>();
			locations.add(new Location(-33.866, 151.195));
			locations.add(new Location(-18.142, 178.431));
			locations.add(new Location(21.291, -157.821));
			map.addPolygon(locations);
			
			//desenez harta. dawMap intoarce o imagine
			img = map.drawMap();
			
			comp.img = img;
		} 
		catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
			e.printStackTrace();			
		}
		
		buildLayout();
	}
	
	JPanel content = new JPanel();
	
	void buildLayout() {
		setContentPane(content);
		content.setLayout(new BorderLayout());
				
		content.add(comp, BorderLayout.CENTER);		
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {			
			@Override
			public void run() {
				new TestFrame();	
			}
		});
	}
	
}

